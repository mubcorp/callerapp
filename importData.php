<?php
include 'connection.php';

if (isset($_POST["import"])) {
    $fileName = $_FILES["file"]["tmp_name"];
    
    if ($_FILES["file"]["size"] > 0) {
        
        $file = fopen($fileName, "r");
        $i = 0;
        while (($column = fgetcsv($file, 1000000, ";")) !== FALSE) {
            if($i == 0)
            {
                $i++;
                continue;
            }
            $hashphone = substr(md5($column[12].$column[0].$column[11].$column[13]),2,13);
            $column[10] = ($column[10]=='')?0 :$column[10];
            $column[11] = ($column[11]=='')?0 :$column[11];
            $column[12] = ($column[12]=='')?0 :$column[12];
            $column[13] = ($column[13]=='')?0 :$column[13];
            $column[14] = (strtolower($column[14])=='y')?1 :0;
            $column[0] = str_replace("'","-",$column[0]);
            $column[1] = str_replace("'","-",$column[1]);
            $column[2] = str_replace("'","-",$column[2]);
            $column[3] = str_replace("'","-",$column[3]);
            $column[6] = str_replace("'","-",$column[6]);
            $column[11] = str_replace("'", "",$column[11]);

            $column[5] = ($column[5]=='')?0 :$column[5];
            $sqlInsert = "INSERT into callerapp (full_name,given_name,last_name,address,premise,street_number,street_name,street_type,suburb,state,postcode,phone,mobile,toll_free,ondncr,hash) values ('" . 
            $column[0] . "','" . $column[1] . "','" . $column[2] . "','" . $column[3] . "','" . $column[4] . "','" . $column[5] . "','" . $column[6] . "','" . $column[7] . "','" . $column[8] . "','" . $column[9] . "','" . $column[10] . "','" . $column[11] . "','" . $column[12] . "','" . $column[13] . "','" . $column[14] . "','" . $hashphone . "')ON DUPLICATE KEY UPDATE `full_name`=`full_name`,`given_name`=`given_name`,`last_name`=`last_name`,`address`=`address`,`premise`=`premise`,`street_number`=`street_number`,`street_name`=`street_name`,`street_type`=`street_type`,`suburb`=`suburb`,`state`=`state`,`postcode`=`postcode`,`phone`=`phone`,`mobile`=`mobile`,`toll_free`=`toll_free`,`ondncr`=`ondncr`,`hash`=`hash`";
            $result = mysqli_query($conn, $sqlInsert);
            if (!empty($result)) {
                $type = "success";
                $message = "CSV Data Imported into the Database";
            } else {
                p(['error' => mysqli_error($conn),
                    'sql' => $sqlInsert
                    ]);
                $type = "error";
                $message = "Problem in Importing CSV Data";
            }
            $i++;
        }

    }
}
?>