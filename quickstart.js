﻿$(function () {
  var speakerDevices = document.getElementById('speaker-devices');
  var ringtoneDevices = document.getElementById('ringtone-devices');
  var outputVolumeBar = document.getElementById('output-volume');
  var inputVolumeBar = document.getElementById('input-volume');
  var volumeIndicators = document.getElementById('volume-indicators');
  log('Requesting Capability Token...');
  $.getJSON('https://razzmatazz-dachshund-5736.twil.io/capability-token')
    .done(function (data) {
      log('Got a token.');
      setClientNameUI(data.identity);
      console.log('Token: ' + data.token);

      // Setup Twilio.Device
      Twilio.Device.setup(data.token);

      Twilio.Device.ready(function (device) {
        log('Device is Ready for a call!');
        var callBtn = document.getElementById('call-controls');
        callBtn.style.display = 'block';
        callBtn.disabled = true;
      });

      Twilio.Device.error(function (error) {
        log('Twilio.Device Error: ' + error.message);
      });

      Twilio.Device.connect(function (conn) {
        log('Successfully established call!');
        document.getElementById('button-call').style.display = 'none';
        document.getElementById('button-hangup').style.display = 'inline';
        volumeIndicators.style.display = 'block';
        bindVolumeIndicators(conn);
      });

      Twilio.Device.disconnect(function (conn) {
        log('Call ended.');
        document.getElementById('button-call').style.display = 'inline';
        document.getElementById('button-hangup').style.display = 'none';
        volumeIndicators.style.display = 'none';
      });

      Twilio.Device.incoming(function (conn) {
        log('Incoming connection from ' + conn.parameters.From);
        var archEnemyPhoneNumber = '+12093373517';

        if (conn.parameters.From === archEnemyPhoneNumber) {
          conn.reject();
          log('It\'s your nemesis. Rejected call.');
        } else {
          // accept the incoming connection and start two-way audio
          conn.accept();
        }
      });

      setClientNameUI(data.identity);

      Twilio.Device.audio.on('deviceChange', updateAllDevices);

      // Show audio selection UI if it is supported by the browser.
      if (Twilio.Device.audio.isSelectionSupported) {
        document.getElementById('output-selection').style.display = 'block';
      }
    })
    .fail(function () {
      log('Could not get a token from server!');
    });

    function callNextNumber(hash=null)
    {
      var xhttp = new XMLHttpRequest();
      xhttp.open("POST","/caller/getNextClient.php", true);
      xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      if(!hash)
      {
        xhttp.send();
      }
      else
      {
        var param = 'hash='+hash;
        xhttp.send(param);
      }
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var response = null;
          var response = this.responseText;
          var data = JSON.parse(CryptoJS.AES.decrypt(JSON.parse(response),'data', {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
          var to = null;
          if((data.phone!= "0")&&(typeof data.phone !== "undefined"))
          {
            to = data.phone;
          }
          else if((data.mobile != "0")&&(typeof data.mobile !== "undefined"))
          {
            to = data.mobile;
          }
          else if((data.toll_free != "0")&&(typeof data.toll_free !== "undefined"))
          {
            to = data.toll_free;
          }
         
          if(to)
          {
            var params = {
              To:to
            };

          var fname = document.getElementById('fname');
          var lname = document.getElementById('lname');
          var address = document.getElementById('address');
          var suburb = document.getElementById('suburb');
          var state = document.getElementById('state');
          var postcode = document.getElementById('postcode');
          // var ondcr = document.getElementById('ondncr');
          if(typeof data.id !== "undefined")
          {
            document.getElementById('recordId').value = data.id;          
          }
          if(typeof data.given_name !== "undefined")
          {
            fname.value = data.given_name;
          }
          else
          {
            fname.value = "";
          }
          if(typeof data.last_name !== "undefined")
          {
            lname.value = data.last_name;
          }
          else
          {
            lname.value = "";
          }
          if(typeof data.address !== "undefined")
          {
            address.value = data.address;
          }
          else
          {
            address.value = "";
          }
          if(typeof data.suburb !== "undefined")
          {
            suburb.value = data.suburb;
          }
          else
          {
            suburb.value = "";
          }
          if(typeof data.state !== "undefined")
          {
            state.value = data.state;
          }
          else
          {
            state.value = "";
          }
          if(typeof data.postcode !== "undefined")
          {
            postcode.value = data.postcode;
          }
          else
          {
            postcode.value = "";
          }
          // ondncr.value = data.ondcr;
            console.log(JSON.stringify(params));
            Twilio.Device.connect(params);
          }
        }
      };
    }

  // Bind button to make call
  document.getElementById('button-call').onclick = function () {
      btnClick = document.getElementById('button-call');
    // get the phone number to connect the call to
     var phoneNumber = document.getElementById('phone-number');

     if(phoneNumber.value != "")
     {
      callNextNumber(phoneNumber.value);
      btnClick.disabled = false;
     }
     else
     {
      callNextNumber();
      btnClick.disabled = false;
     }
     return false;
     };

  // Bind button to hangup call
  document.getElementById('button-hangup').onclick = function () {
    log('Hanging up...');
    Twilio.Device.disconnectAll();
  };

  document.getElementById('get-devices').onclick = function() {
    navigator.mediaDevices.getUserMedia({ audio: true })
      .then(updateAllDevices);
  };

  speakerDevices.addEventListener('change', function() {
    var selectedDevices = [].slice.call(speakerDevices.children)
      .filter(function(node) { return node.selected; })
      .map(function(node) { return node.getAttribute('data-id'); });
    
    Twilio.Device.audio.speakerDevices.set(selectedDevices);
  });

  ringtoneDevices.addEventListener('change', function() {
    var selectedDevices = [].slice.call(ringtoneDevices.children)
      .filter(function(node) { return node.selected; })
      .map(function(node) { return node.getAttribute('data-id'); });
    
    Twilio.Device.audio.ringtoneDevices.set(selectedDevices);
  });

  function bindVolumeIndicators(connection) {
    connection.volume(function(inputVolume, outputVolume) {
      var inputColor = 'red';
      if (inputVolume < .50) {
        inputColor = 'green';
      } else if (inputVolume < .75) {
        inputColor = 'yellow';
      }

      inputVolumeBar.style.width = Math.floor(inputVolume * 300) + 'px';
      inputVolumeBar.style.background = inputColor;

      var outputColor = 'red';
      if (outputVolume < .50) {
        outputColor = 'green';
      } else if (outputVolume < .75) {
        outputColor = 'yellow';
      }

      outputVolumeBar.style.width = Math.floor(outputVolume * 300) + 'px';
      outputVolumeBar.style.background = outputColor;
    });
  }

  function logCallConnected()
  {
    var recordId = document.getElementById('recordId').value;
    if(recordId!=='')
    {
      var xhttp = new XMLHttpRequest();
      xhttp.open("POST","/caller/getNextClient.php", true);
      xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      var param = 'recordId='+recordId;
      xhttp.send(param);
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) 
        {
          log(this.responseText);
        }
      };
    }
  }

  function updateAllDevices() {
    updateDevices(speakerDevices, Twilio.Device.audio.speakerDevices.get());
    updateDevices(ringtoneDevices, Twilio.Device.audio.ringtoneDevices.get());
  }
});

// Update the available ringtone and speaker devices
function updateDevices(selectEl, selectedDevices) {
  selectEl.innerHTML = '';
  Twilio.Device.audio.availableOutputDevices.forEach(function(device, id) {
    var isActive = (selectedDevices.size === 0 && id === 'default');
    selectedDevices.forEach(function(device) {
      if (device.deviceId === id) { isActive = true; }
    });

    var option = document.createElement('option');
    option.label = device.label;
    option.setAttribute('data-id', id);
    if (isActive) {
      option.setAttribute('selected', 'selected');
    }
    selectEl.appendChild(option);
  });
}

// Activity log
function log(message) {
  var logDiv = document.getElementById('log');
  logDiv.innerHTML += '<p>&gt;&nbsp;' + message + '</p>';
  logDiv.scrollTop = logDiv.scrollHeight;
}

// Set the client name in the UI
function setClientNameUI(clientName) {
  var div = document.getElementById('client-name');
  div.innerHTML = 'Your client name: <strong>' + clientName +
    '</strong>';
}
