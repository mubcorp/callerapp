
<!DOCTYPE HTML>
<html>
<head>
<title>CallerApp Make U BIG :: Make U BIG</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Desktop calling widget, call from your website">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="site.css">
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" rel="stylesheet">
<!--//fonts--> 
</head>
<body>
<!--background-->
<!-- login -->
 <?php
function p($data)
{
  echo '<pre>';
  die(print_r($data));
}
$ip = getenv("REMOTE_ADDR") ;

?> 
<!-- <h1 style="mar">Curved Contact Form</h1> -->
    <h1 style="margin-top: 15px; text-decoration: underline; color: #000;">MakeUBIG CallerApp</h1> 
<?php if(($ip == '127.0.0.1')||($ip == '122.180.222.51')){
  ?>
  <div class="container">
   <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-6">
        <div class="login-w3l"> 
          <div class="top-img-agileits-w3layouts">
            <h2 class="sub-head-w3-agileits">Current User Details</h2>
            <p>Please fill the Missing/wrong Info !</p>
            <div class="login-form">  
            <form method="POST" id="userDetails">
              <input type="hidden" name="recordId" id="recordId" value="">
              <div class="row">
              <div class="col-md-6">
              <input type="text" name="fname" id="fname" placeholder="First Name" required="" />
              </div>
              <div class="col-md-6">
              <input type="text" name="lname" id="lname" placeholder="Last Name" required="" />
              </div>
              </div>
              <input type="text" name="address" id="address" placeholder="Adress" required=""/>
              <input type="text" name="suburb" id="suburb" placeholder="Suburb" required="" />
              <input type="text" name="state" id="state" placeholder="State" required="" />
              <input type="text" name="postcode" id="postcode" placeholder="PostCode" required="" />
              <select id="calling_status" name="calling_status">
                <option>Select Calling Status</option>
                <option value="not-called">not-called</option>
                <option value="called">called</option>
                <option value="not-picked">not-picked</option>
                <option value="other">other</option>
              </select>
              <textarea name="message" id="message" placeholder="Message" required=""></textarea>
             <input id="datetime" name="datetime" type="datetime-local">
              <input type="submit" value="Save">
            </form> 
          </div>
          </div>   
        </div> 
      </div>

     <div class="col-md-6 col-lg-6 col-sm-6">
        <div class="login-w3l">  
          <div id="controls">
          <div id="info">
           <p class="instructions">MakeUBIG Client</p>
           <div id="client-name"></div>
           <div id="output-selection">
             <label>Ringtone Devices</label>
             <select id="ringtone-devices" multiple></select>
             <label>Speaker Devices</label>
             <select id="speaker-devices" multiple></select><br/>
             <a id="get-devices">Seeing unknown devices?</a>
           </div>
           </div>

           <div id="log" style="margin-top: 5px;"></div>
            <div id="call-controls" style="margin-top: 3em;">
              <p class="instructions">Make a Call:</p>
              <input id="phone-number" type="text" placeholder="Enter a phone # or client name" />
              <div class="row">
              <div class="col-md-4">
              <button id="button-call">Call</button>
              </div>
              <div class="col-md-4"></div>
              <div class="col-md-4">
              <button id="button-next">Next Call</button>
              </div>
              </div>
              <button id="button-hangup">Hang Up</button>
              <div id="volume-indicators">
                <label>Mic Volume</label>
                <div id="input-volume"></div><br/><br/>
                <label>Speaker Volume</label>
                <div id="output-volume"></div>
              </div>
            </div>
        </div>      
       </div> 
     </div>
     </div>
   </div>
  <?php }?>
  <div class="clear"></div>
  <div class="footer-agileits" style="margin-top: 8em;">
      <p>© 2018 CallerApp. All Rights Reserved | Design & Developed by <a href="http://makeubig.com/" style="color: red;" target="_blank"> Makeubig</a></p>
  </div>
  <script type="text/javascript" src="https://media.twiliocdn.com/sdk/js/client/v1.4/twilio.min.js"></script>
  <script type="text/javascript" src="aes.js"></script>
  <script src="aes-json-format.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="quickstart.js"></script>
  <script>
    $('#userDetails').on('submit',function(e){
      e.preventDefault();
      var form = $('#userDetails').serializeArray();
     $.ajax({
         type:'POST',
         url:"/caller/getNextClient.php",
         data : form,
        success: function(result)
        { 
          alert(result);
        }
    });
    return false;
   })
  </script>
</body>
</html>